<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\CurrencyFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the list builder for order items.
 */
final class OrderItemListBuilder extends EntityListBuilder implements FormInterface {


  /**
   * The parent product.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected OrderInterface $order;

  /**
   * The delta values of the variation field items.
   *
   * @var integer[]
   */
  protected array $orderItemDeltas = [];

  /**
   * Whether tabledrag is enabled.
   *
   * @var bool
   */
  protected bool $hasTableDrag = TRUE;

  /**
   * Constructs a new OrderItemListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\commerce_price\CurrencyFormatter $currencyFormatter
   *   The currency formatter.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected CurrencyFormatter $currencyFormatter,
    EntityRepositoryInterface $entity_repository,
    RouteMatchInterface $route_match,
    protected FormBuilderInterface $formBuilder,
  ) {
    parent::__construct($entity_type, $entityTypeManager->getStorage($entity_type->id()));

    $this->order = $route_match->getParameter('commerce_order');
    // The order might not be available when the list builder is
    // instantiated by Views to build the list of operations. Or just the id
    // might be available in case of contextual filters.
    $this->order = $entity_repository->getTranslationFromContext($this->order);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('commerce_price.currency_formatter'),
      $container->get('entity.repository'),
      $container->get('current_route_match'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_order_items';
  }

  /**
   * {@inheritdoc}
   */
  public function load(): array {
    $order_items = $this->order->getItems();
    foreach ($order_items as $delta => $order_item) {
      $this->orderItemDeltas[$order_item->id()] = $delta;
    }

    return $order_items;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['title'] = $this->t('Title');
    $header['type'] = $this->t('Order item type');
    $header['unit_price'] = $this->t('Unit Price');
    $header['quantity'] = $this->t('Quantity');
    if ($this->hasTableDrag) {
      $header['weight'] = $this->t('Weight');
    }

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];

    $row['#attributes']['class'][] = 'draggable';
    $row['#weight'] = $this->orderItemDeltas[$entity->id()];
    $row['title'] = $entity->label();
    $order_item_type_storage = $this->entityTypeManager->getStorage('commerce_order_item_type');
    $order_item_type = $order_item_type_storage->load($entity->bundle());
    $row['type'] = $order_item_type->label();
    $price = $entity->getUnitPrice();
    $row['unit_price'] = $this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode());
    $row['quantity'] = (int) $entity->getQuantity();
    if ($this->hasTableDrag) {
      $row['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $entity->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $this->orderItemDeltas[$entity->id()],
        '#attributes' => ['class' => ['weight']],
      ];
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = $this->formBuilder->getForm($this);
    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $order_items = $this->load();
    if (count($order_items) <= 1) {
      $this->hasTableDrag = FALSE;
    }
    $delta = 10;
    // Dynamically expand the allowed delta based on the number of entities.
    $count = count($order_items);
    if ($count > 20) {
      $delta = ceil($count / 2);
    }

    $form['order_items'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
    ];
    foreach ($order_items as $entity) {
      $row = $this->buildRow($entity);
      $row['title'] = ['#markup' => $row['title']];
      $row['type'] = ['#markup' => $row['type']];
      $row['unit_price'] = ['#markup' => $row['unit_price']];
      $row['quantity'] = ['#markup' => $row['quantity']];
      if (isset($row['weight'])) {
        $row['weight']['#delta'] = $delta;
      }
      $form['order_items'][$entity->id()] = $row;
    }

    if ($this->hasTableDrag) {
      $form['order_items']['#tabledrag'][] = [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'weight',
      ];
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
        '#button_type' => 'primary',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $order_items = $this->order->getItems();
    $new_order_items = [];
    $order_item_groups = [];
    // Multiple order items can have the same weight, group the order items per
    // weight, and then iterate on order item groups below to reassign a correct
    // weight.
    foreach ($form_state->getValue('order_items') as $id => $value) {
      $order_item_groups[$value['weight']][] = $order_items[$this->orderItemDeltas[$id]];
    }
    ksort($order_item_groups);
    foreach ($order_item_groups as $order_items) {
      foreach ($order_items as $order_item) {
        $new_order_items[] = $order_item;
      }
    }
    $this->order->setItems($new_order_items);
    $this->order->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity): array {
    $operations = parent::getDefaultOperations($entity);
    $route_parameters = [
      'commerce_order' => $this->order->id(),
      'commerce_order_item' => $entity->id(),
    ];

    // We need to override URLs because toUrl method does not extract the order.
    if (isset($operations['edit'])) {
      $operations['edit']['url'] = $this->ensureDestination(Url::fromRoute('entity.commerce_order_item.edit_form', $route_parameters));
    }
    if (isset($operations['delete'])) {
      $operations['delete']['url'] = $this->ensureDestination(Url::fromRoute('entity.commerce_order_item.delete_form', $route_parameters));
    }
    if ($entity->access('create') && $entity->hasLinkTemplate('duplicate-form')) {
      $operations['duplicate'] = [
        'title' => $this->t('Duplicate'),
        'weight' => 20,
        'url' => $this->ensureDestination(Url::fromRoute('entity.commerce_order_item.duplicate_form', $route_parameters)),
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  protected function ensureDestination(Url $url): Url {
    return $url->mergeOptions(['query' => ['destination' => Url::fromRoute('<current>')->toString()]]);
  }

}
