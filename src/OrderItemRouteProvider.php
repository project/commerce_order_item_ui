<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for the order item entity.
 */
class OrderItemRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getAddPageRoute(EntityTypeInterface $entity_type): Route {
    // The add-form route has no bundle argument because the bundle is selected
    // via the product ($product_type->getVariationTypeIds()).
    $route = new Route($entity_type->getLinkTemplate('add-page'));
    $route
      ->setDefaults([
        '_controller' => OrderItemController::class . '::addPage',
        'entity_type_id' => 'commerce_order_item',
        '_title_callback' => OrderItemController::class . '::addTitle',
      ])
      ->setRequirement('_entity_create_any_access', 'commerce_order_item')
      ->setOption('parameters', [
        'commerce_order' => [
          'type' => 'entity:commerce_order',
        ],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type): Route {
    // The add-form route has no bundle argument because the bundle is selected
    // via the product ($product_type->getVariationTypeIds()).
    $route = new Route($entity_type->getLinkTemplate('add-form'));
    $route
      ->setDefaults([
        '_entity_form' => 'commerce_order_item.add',
        'entity_type_id' => 'commerce_order_item',
        '_title_callback' => OrderItemController::class . '::addTitle',
      ])
      ->setRequirement('_order_item_create_access', 'TRUE')
      ->setOption('parameters', [
        'commerce_order' => [
          'type' => 'entity:commerce_order',
        ],
        'commerce_order_item_type' => [
          'type' => 'entity:commerce_order_item_type',
        ],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type): Route {
    $route = new Route($entity_type->getLinkTemplate('collection'));
    $route
      ->addDefaults([
        '_entity_list' => 'commerce_order_item',
        '_title_callback' => OrderItemController::class . '::collectionTitle',
      ])
      ->setRequirement('_order_item_collection_access', 'TRUE')
      ->setOption('parameters', [
        'commerce_order' => [
          'type' => 'entity:commerce_order',
        ],
      ])
      ->setOption('_admin_route', TRUE);

    return $route;
  }

}
