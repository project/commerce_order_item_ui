<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides title callbacks for order item routes.
 */
final class OrderItemController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Constructs a new OrderItemController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RendererInterface $renderer,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Provides the add title callback for order items.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The current commerce order.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The page title.
   */
  public function addTitle(OrderInterface $commerce_order): MarkupInterface {
    return $this->t('Add order item for %label', [
      '%label' => $commerce_order->label(),
    ]);
  }

  /**
   * Provides the callback for adding a product.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The product.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A renderable array, or a redirect response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addPage(OrderInterface $commerce_order): array {
    $entity_type_id = 'commerce_order_item';

    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface[] $order_item_types */
    $order_item_types = $this->entityTypeManager->getStorage('commerce_order_item_type')->loadMultiple();
    $order_item_type_ids = array_keys($order_item_types);

    $form_route_name = 'entity.' . $entity_type_id . '.add_form';

    // Redirect if there's only one bundle available.
    if (count($order_item_type_ids) == 1) {
      $product_variation_type_id = reset($order_item_type_ids);
      return $this->redirect($form_route_name, [
        'commerce_order' => $commerce_order->id(),
        'commerce_order_item_type' => $product_variation_type_id,
      ]);
    }

    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
    ];
    $bundle_entity_type = $this->entityTypeManager->getDefinition('commerce_product_variation_type');

    $build['#cache']['tags'] = $bundle_entity_type->getListCacheTags();

    // Filter out the bundles the user doesn't have access to.
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);
    foreach ($order_item_types as $bundle_name => $bundle) {
      $access = $access_control_handler->createAccess($bundle_name, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($order_item_types[$bundle_name]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Redirect if there's only one bundle available.
    if (count($order_item_types) === 1) {
      $bundle_names = array_keys($order_item_types);
      $bundle_name = reset($bundle_names);
      return $this->redirect($form_route_name, [
        'commerce_product' => $commerce_order->id(),
        'commerce_product_variation_type' => $bundle_name,
      ]);
    }

    // Prepare the #bundles array for the template.
    /** @var \Drupal\commerce_product\Entity\ProductVariationTypeInterface $bundle */
    foreach ($order_item_types as $bundle_name => $bundle) {
      $build['#bundles'][$bundle_name] = [
        'label' => $bundle->label(),
        'description' => $bundle->get('description') ? $bundle->get('description') : '',
        'add_link' => Link::createFromRoute($bundle->label(), $form_route_name, [
          'commerce_order' => $commerce_order->id(),
          'commerce_order_item_type' => $bundle_name,
        ]),
      ];
    }

    return $build;
  }

  /**
   * Provides the collection callback for order items.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The current commerce order.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The page title.
   */
  public function collectionTitle(OrderInterface $commerce_order): MarkupInterface {
    return $this->t('%label order items', [
      '%label' => $commerce_order->label(),
    ]);
  }

  /**
   * Returns a redirect response object for the specified route.
   *
   * @param string $route_name
   *   The name of the route to which to redirect.
   * @param array $route_parameters
   *   (optional) Parameters for the route.
   * @param array $options
   *   (optional) An associative array of additional options.
   * @param int $status
   *   (optional) The HTTP redirect status code for the redirect. The default is
   *   302 Found.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response object that may be returned by the controller.
   */
  protected function redirect(string $route_name, array $route_parameters = [], array $options = [], $status = 302) {
    $options['absolute'] = TRUE;
    return new RedirectResponse(Url::fromRoute($route_name, $route_parameters, $options)->toString(), $status);
  }

}
