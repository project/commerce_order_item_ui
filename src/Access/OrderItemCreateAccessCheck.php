<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for order item creation.
 *
 * Takes the order item type ID from the order type, since an order
 * is always present in order item routes.
 */
class OrderItemCreateAccessCheck implements AccessInterface {

  /**
   * Constructs a new OrderItemCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Checks access to create the order item.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    if (!$order) {
      return AccessResult::forbidden();
    }

    $order_item_type_storage = $this->entityTypeManager->getStorage('commerce_order_item_type');
    $order_item_type_ids = array_keys($order_item_type_storage->loadMultiple());

    $access_control_handler = $this->entityTypeManager->getAccessControlHandler('commerce_order_item');

    /** @var \Drupal\commerce_order\Entity\OrderItemTypeInterface $order_item_type */
    $order_item_type = $route_match->getParameter('commerce_order_item_type');
    if ($order_item_type) {
      if (in_array($order_item_type->id(), $order_item_type_ids, TRUE)) {
        return $access_control_handler->createAccess($order_item_type->id(), $account, [], TRUE);
      }
      else {
        return AccessResult::forbidden();
      }
    }

    return AccessResult::neutral();
  }

}
