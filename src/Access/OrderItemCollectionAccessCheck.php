<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines an access checker for the order item collection route.
 *
 * Takes the order item type ID from the order type, since an order
 * is always present in order item routes.
 */
class OrderItemCollectionAccessCheck implements AccessInterface {

  /**
   * Constructs a new OrderItemCollectionAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Checks access to the order item variation collection.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $order_type_storage = $this->entityTypeManager->getStorage('commerce_order_type');

    if (!$order) {
      return AccessResult::forbidden();
    }

    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
    $order_type = $order_type_storage->load($order->bundle());
    if (!$order_type) {
      return AccessResult::forbidden();
    }

    $order_item_type_storage = $this->entityTypeManager->getStorage('commerce_order_item_type');
    $order_item_types = $order_item_type_storage->loadMultiple();
    // The collection route can be accessed by users with the administer
    // or manage permissions, because those permissions grant full access
    // to order items (add/edit/delete). The route can also be accessed by
    // users with the "access overview" permission, allowing both order item
    // listings to be viewed even if no other operations are allowed.
    $permissions = [
      'administer commerce_order',
      'access commerce_order overview',
    ];
    foreach (array_keys($order_item_types) as $order_item_type_id) {
      $permissions[] = "manage $order_item_type_id commerce_order_item";
    }

    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
