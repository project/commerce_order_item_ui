<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Overrides messages to use "order i" instead of "product variation".
 *
 * This matches the terminology used on other variation routes, which omit
 * the "product" part because it's obvious from the context/url.
 */
class OrderItemDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    $commerce_order_id = $this->getRouteMatch()->getRawParameter('commerce_order');
    // If available, return the collection URL.
    return Url::fromRoute('entity.commerce_order_item.collection', [
      'commerce_order' => $commerce_order_id,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl(): Url {
    $entity = $this->getEntity();
    if ($entity->hasLinkTemplate('collection')) {
      $commerce_order_id = $this->getRouteMatch()->getRawParameter('commerce_order');
      // If available, return the collection URL.
      return Url::fromRoute('entity.commerce_order_item.collection', [
        'commerce_order' => $commerce_order_id,
      ]);
    }
    else {
      // Otherwise fall back to the front page.
      return Url::fromRoute('<front>');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    // Remove the reference from the parent order.
    $order = $this->entity->getOrder();
    if ($order && $order->hasItem($this->entity)) {
      $order->removeItem($this->entity);
      $order->save();
    }
  }

}
