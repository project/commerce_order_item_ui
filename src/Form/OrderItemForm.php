<?php

declare(strict_types=1);

namespace Drupal\commerce_order_item_ui\Form;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\entity\Form\EntityDuplicateFormTrait;

/**
 * Defines the add/edit/duplicate form for order items.
 */
class OrderItemForm extends ContentEntityForm {

  use EntityDuplicateFormTrait;

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id): OrderItemInterface {
    if ($route_match->getRawParameter('commerce_order_item') !== NULL) {
      $entity = $route_match->getParameter('commerce_order_item');
    }
    else {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = $route_match->getParameter('commerce_order');
      /** @var \Drupal\commerce_order\Entity\OrderItemTypeInterface $order_item_type */
      $order_item_type = $route_match->getParameter('commerce_order_item_type');
      $values = [
        'type' => $order_item_type->id(),
        'order_id' => $order->id(),
      ];
      $entity = $this->entityTypeManager->getStorage('commerce_order_item')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state): OrderItemInterface {
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $entity */
    $entity = parent::buildEntity($form, $form_state);

    if ($entity->isNew() && $purchased_entity = $entity->getPurchasedEntity()) {
      $entity->setTitle($purchased_entity->getOrderItemTitle());
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['#tree'] = TRUE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);

    if (isset($actions['delete'])) {
      $actions['delete']['#url'] = Url::fromRoute('entity.commerce_order_item.delete_form', [
        'commerce_order' => $this->entity->getOrderId(),
        'commerce_order_item' => $this->entity->id(),
      ]);
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $save = $this->entity->save();
    $order = $this->entity->getOrder();
    $this->postSave($this->entity, $this->operation);

    if ($order && !$order->hasItem($this->entity)) {
      $order->addItem($this->entity);
    }
    // Force order save to recalculate total price if necessary.
    $order->save();
    $this->messenger()->addMessage($this->t('Saved the %label order item.', ['%label' => $this->entity->label()]));
    $form_state->setRedirectUrl(Url::fromRoute('entity.commerce_order_item.collection', ['commerce_order' => $this->entity->getOrderId()]));

    return $save;
  }

}
